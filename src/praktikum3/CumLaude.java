package praktikum3;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {

		
		double keskmineHinne;
		int l6put66Hinne;

		System.out.println("Palun sisesta keskmine hinne.");
		keskmineHinne = TextIO.getlnDouble();
		if (keskmineHinne < 0 || keskmineHinne > 5) {
			System.out.println("Vigane hinne");
			return;	
		}
		
		System.out.println("Palun sisesta lõputöö hinne.");
		l6put66Hinne = TextIO.getlnInt();
		if (l6put66Hinne < 0 || l6put66Hinne > 5) {
			System.out.println("Vigane hinne");
			return;
		}
		
		if (keskmineHinne > 4.5) {
			if (l6put66Hinne == 5){
			System.out.println("Jah, saad Cum Laude diplomile!");
			return;
			}
		}
		System.out.println("Ei saa!");		

	}

}
