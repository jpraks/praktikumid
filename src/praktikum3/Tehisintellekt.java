package praktikum3;

import lib.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {
		
		 System.out.println("Palun siseta kaks vanust.");
		 int vanus1 = TextIO.getlnInt();
		 int vanus2 = TextIO.getlnInt();
		 
		 int vanusevahe = Math.abs(vanus1 - vanus2);
		 
		 if (vanusevahe > 5) {
			 System.out.println("Valesti");
		 }	else if (vanusevahe > 10) {
			 System.out.println("Väga valesti");
		 } else {
			 System.out.println("Sobib!");
		 }

	}

}
