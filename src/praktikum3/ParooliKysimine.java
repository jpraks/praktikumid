package praktikum3;

import lib.TextIO;

public class ParooliKysimine {

	public static void main(String[] args) {
		
		int sisestusteArv = 0;
		while (true) {	
			String parool = "saladus";
			System.out.println("Sisesta parool");
			String kasutajaSisestus = TextIO.getlnString();
		
			if (parool.equals(kasutajaSisestus)) {
				System.out.println("Õige parool");
				break;
			} else {
				System.out.println("Vale parool");
				sisestusteArv = sisestusteArv + 1;
			}
			
		}
	}

}
