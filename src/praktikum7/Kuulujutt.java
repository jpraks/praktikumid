package praktikum7;

public class Kuulujutt {

	public static void main(String[] args) {
		
		String[] naisenimed = {"Mari", "Kati", "Piret", "Liis"};
		String[] mehenimed = {"Jüri", "Peeter", "Toomas", "Tiit"};
		String[] tegusõnad = {"kõndisid", "jooksid", "sõid", "vaatasid"};
		
		int n = (int) (Math.random() * naisenimed.length);
		int m = (int) (Math.random() * mehenimed.length);
		int t = (int) (Math.random() * tegusõnad.length);
		
		System.out.println(naisenimed[n] + " ja " + mehenimed[m] + " " + tegusõnad[t]);

	}

}
