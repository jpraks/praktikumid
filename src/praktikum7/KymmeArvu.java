package praktikum7;

public class KymmeArvu {

	public static void main(String[] args) {
		
		int arv; //muutuja deklaratsioon
		//int arv = 7;
		int[] arvud = new int[10];
		// int[] arvud = {3, 5, 7, 9, 11};
		
		// väärtustamine
		arv = 7;
		
		arvud[0] = 7;
		arvud[1] = 12;
		
		// väljastamine
		//System.out.println(arv);
		//System.out.println(arvud[1]);
		
		// kogu massiivi saab väljatrükkida for tsükkliga, teisiti ei saa 
		for (int i = 0; i < arvud.length; i++) {
			System.out.println(arvud[i]);
		}

	}

}
