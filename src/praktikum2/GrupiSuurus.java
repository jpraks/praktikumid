package praktikum2;

import lib.TextIO;

public class GrupiSuurus {

	public static void main(String[] args) {
		 
		int arv;
		int suurus;
		
		System.out.println("Palun sisesta inimeste arv");
		arv = TextIO.getlnInt();
		
		System.out.println("Palun sisesta grupi suurus");
		suurus = TextIO.getlnInt();
		
		int gruppideArv = (int) Math.floor(arv / suurus);
		System.out.println("Saab moodustada"  +  gruppideArv +  "gruppi");
		
		int j22k = arv % suurus;
		System.out.println("Jääk on: " + j22k);

	}

}
