package praktikum8;

import lib.TextIO;

public class Inimene {
	
	String nimi;
	int vanus;
	

	public Inimene(String nimi, int vanus) {
//		System.out.println("Olen konstruktormeetod");
		this.nimi = nimi;
		this.vanus = vanus;

	}
	public void tervita() {
		TextIO.putln("Tere, minu nimi on " + nimi + ", olen " + vanus + "-aastane");
		
		
	}
	

}
