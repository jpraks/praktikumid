package praktikum8;

import java.util.ArrayList;

import lib.TextIO;

public class InimesteSisestamine {

	public static void main(String[] args) {
		
		
//		String nimi = new String ("Mati");
//		
//		Inimene keegi = new Inimene("Kati",24);
//		keegi.tervita();
//		
//		Inimene keegiVeel = new Inimene("Juri", 30);
//		 
//		
//		keegiVeel.tervita();
		
		
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		
		while (true){
			System.out.println("Sisesta nimi ja vanus");
			String nimi = TextIO.getlnString();
			if (nimi.equals(""))
				break;
			int vanus = TextIO.getlnInt();
			Inimene keegi = new Inimene(nimi, vanus);
			inimesed.add(keegi);
			
		}
		
		for (Inimene inimene : inimesed) {
		    // Java kutsub välja Inimene klassi toString() meetodi
		    inimene.tervita();
//		    System.out.println(inimene);
		}
		
//		for (int i = 0; i < inimesed.size; i++) {
//			System.out.println(inimesed.get(i));
//			
//		}

	}

}
