package praktikum9;

public class MassiiviSuurimElem {

	public static void main(String[] args) {
		
		int[] massiiv = {1, 3, 6, 7, 8, 3, 5, 7, 21, 3};
		System.out.println(maksimum(massiiv));

	}
	public static int maksimum(int[] massiiv) {
		int max = 0;
		for(int a : massiiv){
			if (a > max){
				max = a;
			}
		}
		return max;
	}

}
