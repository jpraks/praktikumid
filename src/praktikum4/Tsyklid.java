package praktikum4;

public class Tsyklid {

	public static void main(String[] args) {
		
//		if (true) {
//			System.out.println("tingimus on tõene");
//		}
		
//		int arv = 0;
//		
//		while (arv < 3) {
//			System.out.println("tingimus on tõene");
//			System.out.println("arv=" + arv);
//			
//			arv = arv + 1;
//			
//		}
//		System.out.println("while tsükkel lõpetas");
//		System.out.println("arv=" + arv);
		
//		for (int i = 0;i < 3; i++) {
//			System.out.println(i);
		
		// TODO: Trükkida ühele reale numbrid 10 kuni 1
		
		for (int i = 10; i > 0; i = i - 1) {
			System.out.print(i + " ");
		}
		
		System.out.println();
		
		// TODO: Trükkida ühele reale paarisarvud 0 kuni 10 (0,2,4 jne.)
		
		for (int i = 0; i <= 10; i = i + 2) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		for (int i = 0; i <= 10; i++) {
			if (i % 2 == 0)
				System.out.print(i + " ");
		}
		
		System.out.println();
		
		// TODO: Trükkida ühele reale kolmega jaguvad arvud vahemikus 30 kuni 0 (30, 27, 24 jne.)
		
		for (int i = 30; i >= 0; i-=3) {
			System.out.print(i + " ");
		}
		System.out.println();
		System.out.println();
		
		// TODO: tabel
		int tabeliSuurus = 5;
		
		for (int i = 0; i < tabeliSuurus; i++) {
			for (int j = 0; j < tabeliSuurus; j++) {
				if (i == j) {
					System.out.print("1 ");
				} else {
					System.out.print("0 ");
				}
			}
			System.out.println();
		}	
	}

}
