package praktikum10;

public class FibonacciArv {

	public static void main(String[] args) {
		 
//		int v2gaSuurInt = Integer.MAX_VALUE;
//		System.out.println(v2gaSuurInt);
//		v2gaSuurInt++;
//		System.out.println(v2gaSuurInt);
		
		int i =0;
		while (true){
			 System.out.println(i + " - " + fibonacci(i));
			 i++;
		 }
	}
	
	public static long fibonacci(int n) {
		if(n == 0)
			return 0;
		else if(n == 1)
			return 1;
		else
			return fibonacci (n - 1) + fibonacci(n - 2);
		
	}
	

}
