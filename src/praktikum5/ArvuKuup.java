package praktikum5;

import lib.TextIO;

public class ArvuKuup {
	
	public static void main(String[] args) {
		
		System.out.println("Palun sisesta arv");
		int arv = TextIO.getlnInt();
		int arvKuubis = kuup(arv);
		System.out.println(arvKuubis);
		System.out.println(kuup(7));
	}

	public static int kuup(int sisendV22rtus) {
		int tagastusv22rtus = (int) Math.pow(sisendV22rtus, 3);
		return tagastusv22rtus;
	
	}

}
